/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gomesandgedwillo_project1;

import java.util.ArrayList;
import java.util.*;

/**
 *
 * @author s522536
 */
public class GomesAndGedwillo_Project1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("--Iterations: 1000");
        System.out.println("-- List Length: 10000");
        System.out.printf("\t%16s%16s%16s%16s\n", "Insertion", "Merge", "Select", "Built In");
        System.out.print("10\t");
        System.out.printf("%15f%20f%15f%14f\n", timeInsertion(10), timeMerge(10), timeSelect(10), timeBuiltIn(10));
        System.out.print("100\t");
        System.out.printf("%15f%20f%15f%14f\n", timeInsertion(100), timeMerge(100), timeSelect(100), timeBuiltIn(100));
        System.out.print("1000\t");
        System.out.printf("%15f%20f%15f%14f\n", timeInsertion(1000), timeMerge(1000), timeSelect(1000), timeBuiltIn(1000));
        System.out.print("10000\t");
        System.out.printf("%15f%20f%15f%14f\n", timeInsertion(10000), timeMerge(10000), timeSelect(10000), timeBuiltIn(10000));


    }
       public static ArrayList<Integer> randoList(int size)
    {
        ArrayList<Integer> C = new ArrayList<>();
        for (int i=0; i<size; i++)
            C.add((int)(Math.random() * size * 2));
        return C;
    }

    public static int select(ArrayList<Integer> L, int K) {

        ArrayList<Integer> Al = new ArrayList<>();
        ArrayList<Integer> Ae = new ArrayList<>();
        ArrayList<Integer> Ar = new ArrayList<>();

       int randomValue = L.get(0);
        

        for (int i = 0; i < L.size(); i++) {
            if (L.get(i) > randomValue) {
                Ar.add(L.get(i));
            } else if (L.get(i) < randomValue) {
                Al.add(L.get(i));
            } else {
                Ae.add(L.get(i));
            }

        }
        
        if (K < Al.size()) {
            return select(Al, K);
        } else if (K < (Al.size() + Ae.size())) {
            return randomValue;
        }
        else{
           return select(Ar, (K - Al.size()- Ae.size()));
        }

    }
       private static void swapInds(ArrayList<Integer> L, int i, int j)
    {
        Integer t = L.get(i);
        L.set(i, L.get(j));
        L.set(j, t);
    }
   
    
      public static int insertionSort(ArrayList<Integer> L, int K)
    {
        int j;
        for(int i = 0; i < L.size(); i++){
            j = i;
            while (j>0 && L.get(j-1) > L.get(j)){
                swapInds(L, j-1, j);
                j--;
                
            }
        }  
        return L.get(K);
    }
      
      public static int mergeSortKthElement(ArrayList<Integer> L, int K){
          return (mergeSort(L).get(K));
      }
    public static ArrayList<Integer> mergeSort(ArrayList<Integer> L)
    {
        if(L.size() <= 1){
            return L;
        }
        ArrayList<Integer> A = new ArrayList<>();
        A = mergeSort(new ArrayList<>(L.subList(0, L.size()/2)));
        ArrayList<Integer> B = new ArrayList<>();
        B = mergeSort(new ArrayList<>(L.subList(L.size()/2, L.size())));
        
        merge(A, B);

        
        return L ;
    }
      
         private static ArrayList<Integer> merge(ArrayList<Integer> A, ArrayList<Integer> B)
    {
        int iA = 0;
        int iB = 0; 
        ArrayList<Integer> C = new ArrayList<>();
        while (iA < A.size() && iB < B.size()){
            if (A.get(iA) < B.get(iB)){
                C.add(A.get(iA));
                iA++;
            }
            else
                C.add(B.get(iB));
                iB++;
        }
        while(iA < A.size()){
            C.add(A.get(iA));
            iA++;
        }
        while(iB < B.size()){
            C.add(B.get(iB));
            iB++;
        }       
        return C;
        
    }
         
      public static int builtInSort(ArrayList<Integer> L, int K){
          
          Collections.sort(L);
          return L.get(K);
          
      }
      
      
      
          
    public static double timeInsertion(int size)
    {
        double avg = 0;
        for (int i=0; i<30; i++)
        {
            ArrayList<Integer> l = randoList(size);
            int k = (size/2);
            long start = System.nanoTime();
            insertionSort(l,k);
            long end = System.nanoTime();
            avg += (end - start) / 1000000000.0;
        }
        return avg;
    }
        public static double timeMerge(int size)
    {
        double avg = 0;
        for (int i=0; i<30; i++)
        {
            ArrayList<Integer> l = randoList(size);
            int k = (size/2);
            long start = System.nanoTime();
            mergeSortKthElement(l, k);
            long end = System.nanoTime();
            avg += (end - start) / 1000000000.0;
        }
        return avg;
    }
         public static double timeSelect(int size)
    {
        double avg = 0;
        for (int i=0; i<30; i++)
        {
            ArrayList<Integer> l = randoList(size);
            int k = (size/2);
            long start = System.nanoTime();
            select(l, k);
            long end = System.nanoTime();
            avg += (end - start) / 1000000000.0;
        }
        return avg;
    }
        public static double timeBuiltIn(int size)
    {
        double avg = 0;
        for (int i=0; i<30; i++)
        {
            ArrayList<Integer> l = randoList(size);
            int k = (size/2);
            long start = System.nanoTime();
            builtInSort(l, k);
            long end = System.nanoTime();
            avg += (end - start) / 1000000000.0;
        }
        return avg;
    }
      

         

    
}